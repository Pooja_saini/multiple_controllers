//
//  ViewController.m
//  multipleViewControllers
//
//  Created by Click Labs133 on 11/6/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UITextField *textOnMainController;

@end

@implementation ViewController
@synthesize textOnMainController;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.textOnMainController.delegate=self;
//        [self performSegueWithIdentifier:@"push2" sender:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textOnMainController.text isEqual:@"1"]) {
            [self performSegueWithIdentifier:@"push1" sender:nil];
       }

   // [textField resignFirstResponder];
else if ([textOnMainController.text isEqualToString:@"2"]){

    [self performSegueWithIdentifier:@"push2" sender:nil];
}
    return YES;
    
}



@end
